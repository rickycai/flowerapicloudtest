FLOWER.homepage = {

    _autoload: [
        ['initSlick', !!$('.flower-slides').length]
    ],

    initSlick: function() {
        $('.flower-slides').slick({
            dots: true,
            infinite: true,
            autoplay: true,
            speed: 500,
            fade: true,
            cssEase: 'linear'
        });
        $('.flower-art-life-slides').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            adaptiveHeight: true,
            centerMode: true,
            variableWidth: true,
          });
    }
};

FLOWER.location = {
    _autoload: [
        'initUserCurrentLocation'
    ],
    initUserCurrentLocation: function() {
        var provinceInCookie = $.cookie('MOBILE_DEFAULTCITY');
        if (typeof provinceInCookie !== 'undefined') {
            $('.flower-location .text').text(provinceInCookie.name);
        } else {
            //首先设置默认城市
            var defCity = {
                id: '000001',
                name: '北京市',
            };
            //默认城市
            $.cookie('MOBILE_DEFAULTCITY', JSON.stringify(defCity), {
                expires: 1,
                path: '/'
            });
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                        var lat = position.coords.latitude;
                        var lon = position.coords.longitude;
                        var point = new BMap.Point(lon, lat); // 创建点坐标
                        var gc = new BMap.Geocoder();
                        gc.getLocation(point, function(rs) {
                            var addComp = rs.addressComponents;
                            var curCity = {
                                id: '',
                                name: addComp.province
                            };
                            //当前定位城市
                            $.cookie('MOBILE_CURRENTCITY', JSON.stringify(curCity), {
                                expires: 7,
                                path: '/'
                            });
                            if (successFunc != undefined)
                                successFunc(addComp);
                        });
                    },
                    function(error) {
                        switch (error.code) {
                            case 1:
                                alert("位置服务被拒绝。");
                                break;
                            case 2:
                                alert("暂时获取不到位置信息。");
                                break;
                            case 3:
                                alert("获取位置信息超时。");
                                break;
                            default:
                                alert("未知错误。");
                                break;
                        }
                        var curCity = {
                            id: '000001',
                            name: '北京市'
                        };
                        //默认城市
                        $.cookie('MOBILE_DEFAULTCITY', JSON.stringify(curCity), {
                            expires: 1,
                            path: '/'
                        });
                        if (errorFunc != undefined)
                            errorFunc(error);
                    }, {
                        timeout: 5000,
                        enableHighAccuracy: true
                    });
            } else {
                alert("你的浏览器不支持获取地理位置信息。");
            }
        }
    }
}

function _autoload(){
	$.each(FLOWER,function(section,obj){
		if($.isArray(obj._autoload)){
			$.each(obj._autoload,function(key,value){
				if($.isArray(value)){
					if(value[1]){
						FLOWER[section][value[0]]();
					}else{
						if(value[2]){
							FLOWER[section][value[2]]()
						}
					}
				}else{
					FLOWER[section][value]();
				}
			})
		}
	})
}

$(function(){
	_autoload();
});
